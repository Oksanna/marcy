import React from 'react';
import './reset.css'
import './style.css'
import '../common/all_travels.css'
import '../common/videos.css'
import '../common/contacts.css'
import './media.css'
import Header from "./Header/Header.js";
import Blog from "./Blog/Blog.js"
import Footer from "./Footer/Footer.js"



const ListBlog = () =>{
    return(
    <div>
        <Header/>
        <Blog/>
        <Footer/>
    </div>
    
)}


export default ListBlog