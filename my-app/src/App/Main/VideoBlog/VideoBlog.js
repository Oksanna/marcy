import React from 'react';
import YouTube from 'react-youtube';
import {Link} from 'react-router-dom'
import './VideoBlog.css'

class VideoBlog extends React.Component {
    render() {
      const opts = {
        height: '603',
        width: '100%',
        playerVars: { 
          autoplay: 0
        }
      };
   
      return (
        <div>
        <YouTube
          videoId="YtZZ-hxz9TI"
          opts={opts}
          onReady={this._onReady}
          />
        <Link to="/VideoContent" className="description_video" >WATCH ALL   ▶</Link>
        </div>
      );
    }
}

export default VideoBlog