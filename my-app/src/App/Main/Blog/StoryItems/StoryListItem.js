import React from 'react';
import {Link} from 'react-router-dom'
import '../Blog.css'



const StoryListItem = (props) =>{
    return(
            <div className="blog_item">
                <img src={props.image} alt="/" className="blog_card_img"/> 
                    <h3>{props.h3}</h3>
                        <p className="blog_text">
                        {props.description}
                        </p>
                <Link to="/BlogPage" className="storypage_btn">Full story <span>➝</span></Link>
            </div>
    )
}


export default StoryListItem