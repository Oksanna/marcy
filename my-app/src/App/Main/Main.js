import React from 'react';
import {Route} from 'react-router-dom';
import MainStory from './MainStory/MainStory'
import Blog from './Blog/Blog'
import AboutMe from './AboutMe/AboutMe'
import VideoBlog from './VideoBlog/VideoBlog'
import BlogItems from '../Main/BlogItems/BlogItems'
import VideoContent from "../Main/MainStory/VideoContent/VideoContent"
import BlogPage from '../Main/BlogItems/BlogPage/BlogPage'
import '../style.css'



const Main = () =>{
    return(
      <div>   
              <Route path="/" exact component={MainStory}/>
              <Route path="/" exact component={Blog}/>
              <Route path="/" exact component={VideoBlog}/>
              <Route path="/" exact component={AboutMe}/>
              <Route path="/BlogItems" exact component={BlogItems}/>
              <Route path="/VideoContent" exact component={VideoContent}/>
              <Route path="/BlogPage" exact component={BlogPage}/>
      </div>
    )
}

export default Main