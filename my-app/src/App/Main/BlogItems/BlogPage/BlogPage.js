import React from 'react';
// import './BlogPage.css'
import stories from '../../Blog/StoryItems/Stories'



   
const BlogPage = (stories) =>{
    return(
            <div className="main_content_page" key={stories.id}>
                <img src={stories.image} alt="/" className="blog_card_img_page"/> 
                    <h3>{stories.h3}</h3>
                        <p className="blog_text_page">
                        {stories.text}
                        </p>
                </div>
    )

}


export default BlogPage
