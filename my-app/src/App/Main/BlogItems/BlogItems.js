import React from 'react';
import Blog from "../Blog/Blog";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import MonoTrip from './BlogPage/MonoTrip'
import SoloTrip from './BlogPage/SoloTrip'
import './BlogItems.css';


const BlogItems = () =>{
    return(
        <Router>
        <div>
             
        <div className='container filter'>
                <div className="row">
                <Link to="/MonoTrip">Trip with friends</Link>
                <Link to="/SoloTrip">Solo trip</Link>
                <Link to="/BlogItems">All trip</Link>
               
                
                </div>
                </div>
                <Switch>
                                         
                            <Route path="/MonoTrip" exact component={MonoTrip}/>
                            <Route  path="/SoloTrip" exact component={SoloTrip}/> 
                            <Route  path="/BlogItems" exact component={Blog}/>
                       
                 </Switch>
        </div>
        </Router>
    )
}

export default BlogItems