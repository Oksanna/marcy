import React from 'react';
import {Link} from 'react-router-dom'
import './Header.css';
import '../media.css'

const Header = () => {
    return(
        <header>
		<div className="container">
			<div className="row">
				<div className="col-md-2">
					<div className="burger_menu">
						<input id="menu__toggle" type="checkbox" />
						<label className="menu__btn" htmlFor="menu__toggle">
							<span></span>
						</label>
						<ul className="menu__box">
							<li><Link to="/BlogItems" className="menu__item">All travels</Link></li>
							<li><Link to="/VideoContent"  className="menu__item">Videos</Link></li>
						</ul>
					</div>
				</div>
		<div className="col-md-10">
			<div className="header_text">Story</div>
				<Link to="/" className="link_name">
					<div className="name">Marcy.. </div>
				</Link>
			</div>
		</div>
		</div>
		</header>
    )
}

export default Header
